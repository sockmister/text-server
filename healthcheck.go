package main

import (
	"html/template"
	"net/http"
	"os"
)

func handler(w http.ResponseWriter, r *http.Request, data interface{}) {
	t, _ := template.ParseFiles(data.(string))
	t.Execute(w, nil)
}

func makeHandler(fn func(w http.ResponseWriter, r *http.Request, data interface{}), data interface{}) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fn(w, r, data)
	}
}

func main() {
	port := os.Args[1]
	file := os.Args[2]

	http.HandleFunc("/"+file, makeHandler(handler, file))
	http.ListenAndServe(":"+port, nil)
}
